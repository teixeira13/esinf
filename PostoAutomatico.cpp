#include "PostoAutomatico.h"
#include "Posto.h"
#include "Fabrica.h"
#include "Pedido.h"


PostoAutomatico::PostoAutomatico(){
    stockRequisitar=0;
    velOperacao=0;
}

PostoAutomatico::PostoAutomatico(int chave,int stockActual,int stockRequisitar, float velOperacao):Posto(chave,stockActual)
{
    this->stockRequisitar = stockRequisitar;
    this->velOperacao = velOperacao;
}

PostoAutomatico::PostoAutomatico(const PostoAutomatico& p):Posto(p)
{
    this->stockRequisitar = p.stockRequisitar;
    this->velOperacao = p.velOperacao;
}

PostoAutomatico::~PostoAutomatico(){}

int PostoAutomatico::getStockRequesitar() const
{
    return stockRequisitar;
}

float PostoAutomatico::getvelOperacao() const
{
    return velOperacao;
}

bool PostoAutomatico::isOcupado() const
{
    return (velOperacao <= getStockActual());
}

void PostoAutomatico::setStockRequesitar(int stockRequesitar)
{
    this->stockRequisitar = stockRequisitar;
}

void PostoAutomatico::setVelOperacao(float velOperacao)
{
    this->velOperacao = velOperacao;
}

void PostoAutomatico::step(Fabrica& f,int time)
{
    if(getStockActual()>=velOperacao)
    {
        int temp = getStockActual();
        temp -= velOperacao;
        setStockActual(temp);
        if(getStockActual() < velOperacao)
        {
            Pedido* p = new Pedido(this,time,stockRequisitar);
            f.inserePedido(*p);
        }
    }
}

int PostoAutomatico::action(int stockMaxRobot,int stockActualRobot)
{
    int stock = getStockActual();
    int temp = stockRequisitar - stock;
    if(temp > 0)
    {
        if(temp > stockActualRobot)
        {
            stock += stockActualRobot;
            stockActualRobot = 0;
        }else
        {
            stock += temp;
            stockActualRobot -= temp;
        }
    }
    setStockActual(stock);
    return stockActualRobot;
}

PostoAutomatico* PostoAutomatico::clone() const
{
    return new PostoAutomatico(*this);
}

const PostoAutomatico& PostoAutomatico::operator=(const PostoAutomatico& p)
{
    Posto::operator=(p);
    this->stockRequisitar = p.stockRequisitar;
    this->velOperacao = p.velOperacao;

    return *this;
}



void PostoAutomatico::listar(ostream& out) const
{
    out << "Automático";
    Posto::listar(out);
    
}

ostream& operator << (ostream& out, const PostoAutomatico& p)
{
    p.listar(out);
    return out;
}