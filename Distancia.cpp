#include "Distancia.h"

Distancia::Comparacao Distancia::comp = Distancia::Comprimento;

Distancia::Distancia()
{
    metros = 0;
    minutos = 0;
}

Distancia::Distancia(int c,int t)
{
    metros = c;
    minutos = t;
}

Distancia::Distancia(const Distancia &d)
{
    metros = d.metros;
    minutos = d.minutos;
}

Distancia::~Distancia()
{}

void Distancia::setComparacao(Comparacao c)
{
    comp = c;
}

int Distancia::getComprimento() const
{
    return metros;
}

void Distancia::setComprimento(int c)
{
    metros = c;
}

int Distancia::getTempo() const
{
    return minutos;
}

void Distancia::setTempo(int t)
{
    minutos = t;
}


Distancia* Distancia::clone() const
{
    return new Distancia(*this);
}

void Distancia::escreve(ostream& out) const
{
    out << "[" << metros << "|"  << minutos << "]";
}

Distancia Distancia::operator +(Distancia d)
{
    metros += d.metros;
    minutos += d.minutos;
    return *this;
}

bool Distancia::operator !=(const Distancia& d) const
{
    if(comp == Distancia::Comprimento)
        return metros != d.metros;
    return minutos != d.minutos;
}

bool Distancia::operator ==(const Distancia& d) const
{
    if(comp == Distancia::Comprimento)
        return metros == d.metros;
    return minutos == d.minutos;
}

bool Distancia::operator > (const Distancia& d) const
{
    if(comp == Distancia::Comprimento)
        return metros > d.metros;
    return minutos > d.minutos;
}

bool Distancia::operator < (const Distancia& d) const
{
    if(comp == Distancia::Comprimento)
        return metros < d.metros;
    return minutos < d.minutos;
}

const Distancia& Distancia::operator=(const Distancia& d)
{
    if ( this->metros == d.metros && this->minutos==d.minutos)      // é o mesmo objeto?
        return *this;    
    this->metros= d.metros;
    this->minutos=d.minutos;
    return *this;
}

ostream& operator << (ostream &out, const Distancia &d)
{
    d.escreve(out);
    return out;
}
