#ifndef FABRICA_H
#define FABRICA_H

#include "PriorityQueue.h"
#include "Lista.h"
#include "MatrizDist.h"
#include "ListAdjGrafo.h"
#include "Grafo.h"
#include <string>

class Componente;
class Posto;
class Robot;
class Pedido;

using namespace std;

class Fabrica
{
    public:
        Fabrica();
        Fabrica(const Fabrica& f);
        virtual ~Fabrica();
        
        string setupFabrica(const string& file1,const string& file2,const string& file3,const string& file4);
        const MatrizDist getMatriz() const;
        const MatrizDist getMatrizMinComprimento() const;
        const MatrizDist getMatrizMinTempo() const;
        
        const Lista<Posto*> getPostos() const;
        const Lista<Robot*> getRobots() const;
        const PriorityQueue<Pedido>& getPedidos()const;
        const Queue<Pedido>& getHistoricoPedidos() const;
        void step(int time);
        void inserePedido(const Pedido& p);
        void arquivaPedido(const Pedido& p);
        void escreve(ostream& out) const;
        bool vazia() const;
        
        const Grafo& getGrafo() const; 
        bool validaGrafo() const;
        bool operator >(const Fabrica& f) const;
        const Fabrica& operator =(const Fabrica& f);
        
    
    private:
        Lista<Posto *> postos;
        PriorityQueue<Pedido> pedidos;
        Queue<Pedido> historico;
        Lista<Robot *> robots;
        Lista<Componente *> emTrabalho;
        MatrizDist distancias;
        MatrizDist minComprimento;
        MatrizDist minTempo;
        Grafo grafo;
        
        void criarGrafo();
        void criarMatrizesMinimas();
        void carregarRobots(int time);
        void atendePedidos(int time);
        void iniciaTrabalho();
};

ostream& operator << (ostream& out,const  Fabrica& f);
#endif // FABRICA_H
