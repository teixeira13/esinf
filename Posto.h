#ifndef POSTO_H
#define POSTO_H
#include <iostream>
#include "Componente.h"

class Fabrica;

using namespace std;

class Posto : public Componente
{
    public:
        Posto();
        Posto(int chave,int stockActual);
        Posto(const Posto& p);
        virtual ~Posto();
        void setStockActual(int stock);
        int getStockActual() const;
        virtual void listar(ostream& out) const;
        virtual bool isOcupado() const;
        
        virtual Posto* clone() const = 0;
        virtual void step(Fabrica& f,int time)=0;
        virtual int action(int stockMaxRobot,int stockActualRobot) = 0;

        virtual bool operator ==(const Posto& p) const;
        virtual bool operator >(const Posto& p) const;
        virtual const Posto& operator = (const Posto& p);
    private:
        int stockActual;

};

ostream& operator << (ostream& out, const Posto& p);

#endif // POSTO_H
