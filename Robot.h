#ifndef ROBOT_H
#define ROBOT_H

#include "Posto.h"
#include "Componente.h"
#include "Stack.h"

class Fabrica;

class Robot : public Componente
{
    public:
        Robot();
        Robot(int chave);
        Robot(int chave,int capacidadeMaxima,int capacidadeActual,Posto& p);
        Robot(const Robot& r);
        ~Robot();

        Robot* clone() const;
        int getCapacidadeMaxima() const;
        int getCapacidadeActual() const;
        int getTempoProx() const;
        bool isOcupado() const;
        const Posto& getPosicao() const;
        void setCapacidadeMaxima(int capacidadeMaxima);
        void setCapacidadeActual(int capacidadeActual);
        void setPosicao(Posto& p);
        void arranca(Fabrica& f,Posto* destino,int time);
        void step(Fabrica& f,int time);
        void escreve(ostream& out) const;
        bool operator!=(const Robot& r) const;
        bool operator==(const Robot& r) const;
        bool operator > (const Robot& r) const;
        const Robot& operator=(const Robot& r);
    

    private:
        int capacidadeMaxima;
        int capacidadeActual;
        int tempoProx;
        Posto * posicao;
        Stack<Posto *> caminho;
};

ostream& operator << (ostream &out, const Robot &r);


#endif // ROBOT_H
