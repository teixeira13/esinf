#include "IOFabrica.h"
#include "PostoArmazem.h"
#include "PostoAutomatico.h"
#include "Lista_Iterador.h"
#include "Lista.h"
#include "Distancia.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;


namespace IOFabrica
{   
    string lerPostosArmazem(Lista<Posto*>& postos,Lista<Robot*>& robots,const string& file)
    {
        int count = 0;
        string palavra;
        ifstream ficheiro(file.c_str());
        if(ficheiro.is_open())
        {
            ficheiro.seekg(0,ios::end);
            long end = ficheiro.tellg();
            ficheiro.seekg(0,ios::beg);
            getline(ficheiro,palavra);
            long position = ficheiro.tellg();
            while(position < end)
            {
                count ++;
                int chave,stockActual,stockSeguranca,chaveRobot;
                Robot* robot;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> chave;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> stockActual;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> stockSeguranca;
                getline(ficheiro,palavra);
                stringstream(palavra) >> chaveRobot;
                robot = new Robot(chaveRobot);
                robots.insere(count,robot);
                postos.insere(count,new PostoArmazem(chave,stockActual,stockSeguranca,*robot));
                position = ficheiro.tellg();
            }
            ficheiro.close();
        }else
            return "O Ficheiro não Abriu\n";
        stringstream str;
        str << count << " Postos Armazem Inseridos" << endl;
        return str.str();
    }
    
    string lerPostosAutomaticos(Lista<Posto*>& postos, const string& file)
    {
        int count = 0;
        string palavra;
        ifstream ficheiro(file.c_str());
        if(ficheiro.is_open())
        {
            ficheiro.seekg(0,ios::end);
            long end = ficheiro.tellg();
            ficheiro.seekg(0,ios::beg);
            long position = ficheiro.tellg();
            while(position < end)
            {
                count ++;
                int chave,stockActual,quantRequesicao,velocidade;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> chave;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> stockActual;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> quantRequesicao;
                getline(ficheiro,palavra);
                stringstream(palavra) >> velocidade;
                postos.insere(count,new PostoAutomatico(chave,stockActual,quantRequesicao,velocidade));
                position = ficheiro.tellg();
            }
            ficheiro.close();
        }else
            return "O Ficheiro não Abriu\n";
        stringstream str;
        str << count << " Postos Automaticos Inseridos" << endl;
        return str.str();
    }
    
    string lerRobots(Lista<Robot*>& robots,Lista<Posto*>& postos,const string& file)
    {
        int count = 0;
        string palavra;
        ifstream ficheiro(file.c_str());
        if(ficheiro.is_open())
        {
            ficheiro.seekg(0,ios::end);
            long end = ficheiro.tellg();
            ficheiro.seekg(0,ios::beg);
            long position = ficheiro.tellg();
            while(position < end)
            {
                int chave,capacidadeActual,capacidadeMaxima,chavePosto;
                Lista_Iterador<Robot*> itr(robots);
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> chave;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> capacidadeMaxima;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> capacidadeActual;
                getline(ficheiro,palavra);
                stringstream(palavra) >> chavePosto;
                position = ficheiro.tellg();
                while(!itr.fim())
                {
                    if((*itr)->getChave() == chave)
                    {
                        (*itr)->setCapacidadeActual(capacidadeActual);
                        (*itr)->setCapacidadeMaxima(capacidadeMaxima);
                        Lista_Iterador<Posto*> itp(postos);
                        while(!itp.fim())
                        {
                            if((*itp)->getChave() == chavePosto)
                            {
                                (*itr)->setPosicao(**itp);
                                break;
                            }
                            itp++;
                        }
                    }
                    itr++;
                }
                count ++;
            }
            ficheiro.close();
        }else
            return "O Ficheiro não Abriu\n";
        stringstream str;
        str << count << " Robots Inseridos" << endl;
        return str.str();
    }
    
    string lerDistancias(MatrizDist& matriz, Lista<Posto*> postos, const string& file)
    {
        int count = 0;
        string palavra;
        ifstream ficheiro(file.c_str());
        if(ficheiro.is_open())
        {
            ficheiro.seekg(0,ios::end);
            long end = ficheiro.tellg();
            ficheiro.seekg(0,ios::beg);
            long position = ficheiro.tellg();
            while(position < end)
            {
                int chaveP1=0,chaveP2=0,centimetros,minutos;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> chaveP1;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> chaveP2;
                getline(ficheiro,palavra,',');
                stringstream(palavra) >> centimetros;
                getline(ficheiro,palavra);
                stringstream(palavra) >> minutos;
                Distancia d(centimetros,minutos);
                Posto* p1;
                Posto* p2;
                Lista_Iterador<Posto*> it1(postos);
                while(!it1.fim())
                {
                    if((*it1)->getChave() == chaveP1)
                    {
                        p1 = *it1;
                        break;
                    }
                    it1++;
                }
                Lista_Iterador<Posto*> it2(postos);
                while(!it2.fim())
                {
                    if((*it2)->getChave() == chaveP2)
                    {
                        p2 = (*it2);
                        break;
                    }
                    it2++;
                }
                matriz.insere(d,*p1,*p2);
                count ++;
                position = ficheiro.tellg();
            }
            ficheiro.close();
        }else
            return "O Ficheiro não Abriu\n";
        stringstream str;
        str << count << " Distancias Inseridas" << endl;
        return str.str();
    }
}
