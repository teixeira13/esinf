#ifndef PEDIDO_H
#define	PEDIDO_H

#include <iostream>
#include "Queue.h"

class Posto;
class Robot;

using namespace std;

class Pedido {
public:
    Pedido();
    Pedido(Posto* posto, int timePedido,int quantidadeRequisicao);
    Pedido(const Pedido& orig);
    virtual ~Pedido();
    
    void setQuantidadeRequisicao(int q);
    int  getQuantidadeRequisicao() const;
    Posto* getPosto()const;
    void setLock(bool l);
    int getTimePedido() const;
    void setTimeDespachado(int t);
    int getTimeDespachado() const;
    const Queue<Robot>& getHistorico() const;
    bool isLocked()const;
    void insereRobot(const Robot& r);

    bool operator ==(const Pedido& p) const;
    bool operator>(const Pedido& p) const;
    bool operator >=(const Pedido& p) const;
    bool operator<(const Pedido& p) const;
    bool operator <=(const Pedido& p) const;
    const Pedido& operator=(const Pedido& p);
    
    void escreve(ostream& out) const;
    
private:
    Posto* posto;
    bool lock;
    int timePedido;
    int timeDespachado;
    int quantidadeRequisicao;
    Queue<Robot> historico;
};

ostream& operator<<(ostream& out,const Pedido& p);
#endif	/* PEDIDO_H */

