#include "PostoArmazem.h"
#include "Posto.h"
#include "Robot.h"
#include "Fabrica.h"
#include "Pedido.h"


PostoArmazem::PostoArmazem():Posto(0,0)
{
    stockMaximo=0;
    stockSeguranca=0;
    robot = NULL;
}

PostoArmazem::PostoArmazem(int chave, int stockActual, int stockSeguranca,Robot& robot):Posto(chave,stockActual)
{
    this->stockSeguranca = stockSeguranca;
    this->stockMaximo = stockActual;
    this->robot = &robot;
    this->emEspera = false;
}

PostoArmazem::PostoArmazem(const PostoArmazem& p):Posto(p)
{
    this->stockSeguranca = p.stockSeguranca;
    this->stockMaximo = p.stockMaximo;
    robot = p.robot;
}

PostoArmazem::~PostoArmazem()
{}

int PostoArmazem::getStockMaximo() const
{
    return stockMaximo;
}

int PostoArmazem::getStockSeguranca() const
{
    return stockSeguranca;
}

bool PostoArmazem::isEmEspera() const
{
    return emEspera;
}

const Robot& PostoArmazem::getRobot() const
{
    return *robot;
}

void PostoArmazem::setStockMaximo(int stockMaximo)
{
    this->stockMaximo = stockMaximo;
}

void PostoArmazem::setStockSeguranca(int stockSeguranca)
{
    this->stockSeguranca = stockSeguranca;
}

void PostoArmazem::setRobot(Robot& r)
{
    robot = &r;
}

void PostoArmazem::step(Fabrica& f,int time)
{
    if(!emEspera)
        if(getStockActual() <= stockSeguranca)
        {
            Pedido* p = new Pedido(this,time,(3*stockSeguranca));
            f.inserePedido(*p);
            emEspera = true;
        }
}

int PostoArmazem::action(int stockMaxRobot,int stockActualRobot)
{
    if(stockActualRobot == 0)
    {
        //Carrega robot
        if(stockMaxRobot < getStockActual())
        {
            stockActualRobot = stockMaxRobot;
            int temp = getStockActual();
            temp -= stockMaxRobot;
            setStockActual(temp);
        }else
        {
            stockActualRobot += getStockActual();
            setStockActual(0);
        }
    }else
    {
        // Descarrega robot
        int needed = (3*stockSeguranca)-getStockActual();
        if(needed > 0)
        {
            int temp = getStockActual();
            if(needed > stockActualRobot)
            {
                temp += stockMaxRobot;
                stockActualRobot = 0;
            }else
            {
                temp += needed;
                stockActualRobot -= needed;
                emEspera = false;
            }
            setStockActual(temp);
        }
    }
    return stockActualRobot;
}

PostoArmazem* PostoArmazem::clone() const
{
    return new PostoArmazem(*this);
}

const PostoArmazem& PostoArmazem::operator=(const PostoArmazem& p)
{
    Posto::operator=(p);
    if(robot != NULL)
        delete robot;

    stockMaximo = p.stockMaximo;
    stockSeguranca = p.stockSeguranca;
    robot = p.robot->clone();
    return *this;
}

void PostoArmazem::listar(ostream& out) const
{
    out << "Armazém";
    Posto::listar(out);
}

ostream& operator << (ostream& out, const PostoArmazem& p)
{
    p.listar(out);
    return out;
}


