#include "Robot.h"
#include "Posto.h"
#include "Fabrica.h"
#include "Distancia.h"
#include <iostream>
using namespace std;


Robot::Robot(): Componente(), capacidadeMaxima(),  capacidadeActual(){
    posicao = NULL;
}

Robot::Robot(int chave):Componente(chave)
{
    capacidadeActual = 0;
    capacidadeMaxima = 0;
    posicao = NULL;
}

Robot::Robot(int chave, int capacidadeMaxima,int capacidadeActual,Posto& p) : Componente(chave)
{
    this->capacidadeMaxima = capacidadeMaxima;
    this->capacidadeActual = capacidadeActual;
    posicao = &p;
}

Robot::Robot(const Robot& r) : Componente(r)
{
    this->capacidadeMaxima = r.capacidadeMaxima;
    this->capacidadeActual = r.capacidadeActual;
    this->posicao = r.posicao;
}

Robot::~Robot()
{}

Robot* Robot::clone() const
{
    return new Robot(*this);
}

int Robot::getCapacidadeMaxima() const
{
    return capacidadeMaxima;
}

int Robot::getCapacidadeActual() const
{
    return capacidadeActual;
}

const Posto& Robot::getPosicao() const
{
    return *posicao;
}

int Robot::getTempoProx() const
{
    return tempoProx;
}

bool Robot::isOcupado() const
{
    if(caminho.vazia())
        return false;
    return true;
}

void Robot::setCapacidadeMaxima(int capacidadeMaxima)
{
    this->capacidadeMaxima = capacidadeMaxima;
}

void Robot::setCapacidadeActual(int capacidadeActual)
{
    this->capacidadeActual = capacidadeActual;
}

void Robot::setPosicao(Posto& p)
{
    posicao = &p;
}

void Robot::arranca(Fabrica& f, Posto* destino,int time)
{
    f.getGrafo().Dijkstra(posicao->getChave(),destino->getChave(),caminho,Distancia::Comprimento);
    tempoProx = 0;
}

void Robot::step(Fabrica& f,int time)
{
    if(tempoProx > 1)
    {
        tempoProx --;
        return;
    }
    
    tempoProx --;
    caminho.pop(posicao);
    
    if(!caminho.vazia())
        tempoProx = f.getMatriz().getDistancia(*posicao,*(caminho.top())).getTempo();
    else
       capacidadeActual = posicao->action(capacidadeMaxima,capacidadeActual);
}

void Robot::escreve(ostream& out) const
{
    out << "Robot[" << getChave() << "]: " << "Posicao[" << posicao->getChave() << "] Stock[" << capacidadeActual << "/" << capacidadeMaxima << "]";
    if(isOcupado())
    {
        out << " Caminho[";
        Stack<Posto*> temp;
        temp = caminho;
        bool flag = false;
        while(!temp.vazia())
        {
            Posto* p;
            if(flag)
                out << "|";
            flag = true;
            temp.pop(p);
            out << p->getChave();
        }
        out << "] Tempo Próximo[" << tempoProx << "]";
    }else
        out << " Livre!";

}
 
bool Robot::operator !=(const Robot& r) const
{
    return capacidadeActual!= r.capacidadeActual;
}
 
bool Robot::operator ==(const Robot& r) const
{
    return  capacidadeActual == r.capacidadeActual;
}
 
bool Robot::operator > (const Robot& r) const
{
    return capacidadeActual > r.capacidadeActual;
}
 
const Robot& Robot::operator=(const Robot& r)
{
    Componente::operator =(r);
    this->capacidadeMaxima=r.capacidadeMaxima;
    this->capacidadeActual=r.capacidadeActual;
    this->caminho = r.caminho;
    this->posicao = r.posicao;
    this->tempoProx = r.tempoProx;
    return *this;
}

ostream& operator << (ostream &out, const Robot &r)
{
    r.escreve(out);
    return out;
}


