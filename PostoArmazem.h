#ifndef POSTOARMAZEM_H
#define POSTOARMAZEM_H

#include "Posto.h"
#include "Robot.h"

class Fabrica;

class PostoArmazem : public Posto
{
    public:
        PostoArmazem();
        PostoArmazem(int chave, int stockActual, int stockSeguranca,Robot& robot);
        PostoArmazem(const PostoArmazem& p);
        ~PostoArmazem();

        int getStockMaximo() const;
        int getStockSeguranca() const;
        bool isEmEspera() const;
        const Robot& getRobot() const;
        void setStockMaximo(int stockMaximo);
        void setStockSeguranca(int stockSeguranca);
        void setRobot(Robot& r);
        void step(Fabrica& f,int time);
        int action(int stockMaxRobot,int stockActualRobot);
        PostoArmazem* clone() const;
        void listar(ostream& out) const;
        const PostoArmazem& operator=(const PostoArmazem& p);
    private:
        int stockSeguranca;
        int stockMaximo;
        bool emEspera;
        Robot * robot;
};

ostream& operator << (ostream& out, const PostoArmazem& p);

#endif // POSTOARMAZEM_H


