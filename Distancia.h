#ifndef DISTANCIA_H
#define DISTANCIA_H
#include <iostream>

using namespace std;


class Distancia
{
    public:
        Distancia();
        Distancia(int c,int t);
        Distancia(const Distancia &d);
        ~Distancia();
        
        enum Comparacao{Comprimento,Tempo};
        static void setComparacao(Comparacao c);

        int getComprimento() const;
        void setComprimento(int c);
        int getTempo() const;
        void setTempo(int t);
        Distancia* clone() const;
        void escreve(ostream& out) const;
        Distancia operator+(Distancia d);
        bool operator!=(const Distancia& d) const;
        bool operator==(const Distancia& d) const;
        bool operator > (const Distancia& d) const;
        bool operator < (const Distancia& d) const;
        const Distancia& operator=(const Distancia& d);

    private:
        int metros;
        int minutos;
        static Comparacao comp;
};

ostream& operator << (ostream &out, const Distancia &d);

#endif // DISTANCIA_H
