#ifndef IOFABRICA_H
#define IOFABRICA_H

#include <string>
#include "Queue.h"
#include "Posto.h"
#include "Robot.h"
#include "Lista.h"
#include "MatrizDist.h"
#include "Queue.h"

namespace IOFabrica
{
    void lerFicheiro1();
    void lerFicheiro2();
    string lerPostosArmazem(Lista<Posto*>& postos,Lista<Robot*>& robots,const string& file);
    string lerPostosAutomaticos(Lista<Posto*>& postos,const string& file);
    string lerRobots(Lista<Robot*>& robots,Lista<Posto*>& postos,const string& file);
    string lerDistancias(MatrizDist& matriz, Lista<Posto*> postos, const string& file);
}

#endif // IOFABRICA_H
