#include "Componente.h"

Componente::Componente(){
    this->chave = 0;
}

Componente::Componente(const Componente& orig)
{
    this->chave = orig.chave;
}

Componente::Componente(int chave)
{
    this->chave = chave;
}

Componente::~Componente() {}

int Componente::getChave() const
{
    return chave;
}

void Componente::setChave(int chave)
{
    this->chave = chave;
}

void Componente::listar(ostream& out) const
{
    out << "Chave: " << chave;
}

const Componente& Componente::operator =(const Componente& c)
{
    if(this->chave == c.chave)
        return *this;
    this->chave = c.chave;
    return *this;
}

ostream& operator <<(ostream& out,const Componente& c)
{
    c.listar(out);
    return out;
}