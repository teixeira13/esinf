#ifndef COMPONENTE_H
#define	COMPONENTE_H

#include <iostream>
class Fabrica;

using namespace std;

class Componente {
public:
    Componente();
    Componente(int chave);
    Componente(const Componente& orig);
    virtual ~Componente();
    int getChave() const;
    void setChave(int chave);
    virtual bool isOcupado() const = 0;
    virtual void step(Fabrica& f,int time) = 0;
    virtual void listar(ostream& out) const;
    virtual const Componente& operator =(const Componente& c);
private:
    int chave;
};

ostream& operator <<(ostream& out,const Componente& c);

#endif	/* COMPONENTE_H */

