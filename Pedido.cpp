#include "Pedido.h"
#include "PostoArmazem.h"
#include "PostoAutomatico.h"
#include "Queue.h"
#include <typeinfo>

Pedido::Pedido() {}

Pedido::Pedido(Posto* posto, int timePedido, int quantidadeRequisicao)
{
    this->posto = posto;
    this->timePedido = timePedido;
    this->quantidadeRequisicao = quantidadeRequisicao;
    lock = false;
    timeDespachado = 0;
}

Pedido::Pedido(const Pedido& orig) 
{
    posto = orig.posto;
    timePedido = orig.timePedido;
    quantidadeRequisicao = orig.quantidadeRequisicao;
    this->lock = orig.lock;
    timeDespachado = orig.timeDespachado;
    historico = orig.historico;
}

Pedido::~Pedido() {}

Posto* Pedido::getPosto() const
{
    return posto;
}

int Pedido::getQuantidadeRequisicao() const
{
    return quantidadeRequisicao;
}

void Pedido::setQuantidadeRequisicao(int q)
{
   quantidadeRequisicao = q;
}

int Pedido::getTimePedido()const
{
    return timePedido;
}

void Pedido::setTimeDespachado(int t)
{
    timeDespachado = t;
}

int Pedido::getTimeDespachado() const
{
    return timeDespachado;
}

const Queue<Robot>& Pedido::getHistorico() const
{
    return historico;
}

bool Pedido::isLocked() const
{
    return lock;
}

void Pedido::setLock(bool l)
{
    lock = l;
}

void Pedido::insereRobot(const Robot& r)
{
    Robot* temp = r.clone();
    historico.insere(*temp);
}

bool Pedido::operator >(const Pedido& p) const
{
    if(lock)
        return true;
    if((typeid(*(this->posto))== typeid(PostoAutomatico) && typeid(*(p.posto)) == typeid(PostoArmazem)) || (typeid(*(this->posto))==typeid(*(p.posto))))
        return false;
    return true;
}

bool Pedido::operator ==(const Pedido& p) const
{
    return (typeid(*(this->posto)) == (typeid(*(p.posto))) && (this->lock == p.lock));
}

bool Pedido::operator <(const Pedido& p) const
{
    if(p.lock)
        return true;
    return !operator>(p) && !operator==(p);
}

bool Pedido::operator <=(const Pedido& p) const
{
    return operator <(p) || operator <=(p);
}

bool Pedido::operator >=(const Pedido& p) const
{
    return operator >(p) || operator ==(p);
}

const Pedido& Pedido::operator=(const Pedido &p)
{
    posto = p.posto;
    lock = p.lock;
    timePedido = p.timePedido;
    timeDespachado = p.timeDespachado;
    quantidadeRequisicao = p.quantidadeRequisicao;
    historico = p.historico;
    return *this;
}

void Pedido::escreve(ostream& out) const
{
    out << "Posto[" << posto->getChave() << "] Quantidade em falta[" << quantidadeRequisicao << "]" << endl;
}

ostream& operator<<(ostream& out,const Pedido& p)
{
    p.escreve(out);
    return out;
}