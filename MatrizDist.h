#ifndef MATRIZ_H
#define MATRIZ_H
#include<iostream>
class Distancia;
class Posto;

using namespace std;

class MatrizDist
{
    public:
        MatrizDist();
        MatrizDist(int tamanho,bool simetrica);
        MatrizDist(const MatrizDist &m);
        ~MatrizDist();

        void insere(const Distancia &d,const Posto &p1,const Posto &p2);
        const Distancia& getDistancia(const Posto& p1,const Posto& p2) const;
        int getNumeroPostos() const;
        int getTamanho() const;
        bool isSimetrica() const;
        ostream& escreve(ostream& out) const;
        const MatrizDist& operator=(const MatrizDist& m);
        bool vazia() const;
    private:
        static Distancia distanciaZero;
        static Distancia distanciaNula;
        int tamanho;
        int actual;
        bool simetrica;
        Distancia* **distancias;
        int *postos;

        void aumentarTamanho();
        Distancia*** criarMatriz(int tamanho);
        int descobrirPosicao(const Posto &p) const;
        int novoPosto(const Posto &p);

};

ostream& operator << (ostream& out, const MatrizDist& p);

#endif // MATRIZ_H
