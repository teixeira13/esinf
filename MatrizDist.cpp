#include "MatrizDist.h"
#include "Distancia.h"
#include "Posto.h"

//Distancia zero, usada para indicar a distancia de um posto a si mesmo
Distancia MatrizDist::distanciaZero(0,0);
//Distancia nula, usada quando não existe caminho de um posto ao outro
Distancia MatrizDist::distanciaNula(-1,-1);

MatrizDist::MatrizDist()
{
    tamanho = 0;
    actual = 0;
    simetrica = true;
    distancias = NULL;
    postos = NULL;
}

MatrizDist::MatrizDist(int tamanho, bool simetrica)
{
    this->tamanho = tamanho;
    this->actual = 0;
    this->simetrica = simetrica;

    distancias = criarMatriz(tamanho);

    postos = new int[tamanho];
}

MatrizDist::MatrizDist(const MatrizDist &m)
{
    tamanho = m.tamanho;
    actual = m.actual;
    simetrica = m.simetrica;

    distancias = criarMatriz(tamanho);
    postos = new int[tamanho];

    for(int i = 0; i < actual; i++)
    {
        for(int j = 0;j < actual; j++)
        {
            if(m.distancias[i][j] != NULL)
                distancias[i][j] = m.distancias[i][j]->clone();
        }
    }

    for(int i = 0; i < actual; i++)
        postos[i] = m.postos[i];

}

MatrizDist::~MatrizDist()
{
    for(int i = 0; i < actual; i++)
    {
        if(simetrica)
        {
            for(int j=i; j<actual;j++)
            {
                if((distancias[i][j] != &distanciaNula) && (distancias[i][j] != &distanciaZero))
                    delete distancias[i][j];
            }
        }else
        {
            for(int j=0; j<actual;j++)
            {
                if((distancias[i][j] != &distanciaNula) && (distancias[i][j] != &distanciaZero))
                    delete distancias[i][j];
            }
        }
        delete [] distancias[i];
    }

    delete [] postos;
    delete [] distancias;
}

int MatrizDist::getNumeroPostos() const
{
    return actual;
}

int MatrizDist::getTamanho() const
{
    return tamanho;
}

bool MatrizDist::isSimetrica() const
{
    return simetrica;
}

//insere a distancia entre dois pontos na matriz
void MatrizDist::insere(const Distancia &d,const Posto &p1,const Posto &p2)
{
    int pos1 = descobrirPosicao(p1);
    if(pos1 < 0)
        pos1 = novoPosto(p1);
    
    int pos2 = descobrirPosicao(p2);
    if(pos2 < 0)
        pos2 = novoPosto(p2);

    if(simetrica)
    {
        distancias[pos1][pos2] = distancias[pos2][pos1] = d.clone();
    }else
    {
        distancias[pos1][pos2] = d.clone();
    }

}

//Devolve a distancia entre dois postos, caso algum dos postos não exista devolve a distancia nula
const Distancia& MatrizDist::getDistancia(const Posto& p1, const Posto& p2) const
{
    int pos1 = descobrirPosicao(p1);
    if(pos1 < 0)
        return distanciaNula;
    
    int pos2 = descobrirPosicao(p2);
    if(pos2 < 0)
        return distanciaNula;
    
    return *distancias[pos1][pos2];
}

//Duplica o tamanho da matriz e do vector
void MatrizDist::aumentarTamanho()
{
    tamanho = (tamanho == 0 ? 15 : tamanho*2) ;
    Distancia** *aux = criarMatriz(tamanho);

    int* auxPostos = new int[tamanho];


    for(int i=0; i<actual;i++)
    {
        auxPostos[i] = postos[i];
        if(simetrica)
        {
            for(int j=i; j<actual; j++)
            {
                aux[i][j] = aux[j][i] = distancias[i][j];
            }
        }else
        {
            for(int j=0; j<actual; j++)
            {
                aux[i][j] = distancias[i][j];
            }
        }
        delete [] distancias[i];
    }

    delete [] distancias;
    delete [] postos;
    distancias = aux;
    postos = auxPostos;
}

//Descobre o index de um posto na matriz se não existir acrescenta
int MatrizDist::descobrirPosicao(const Posto &p) const
{
    for(int i = 0; i < actual; i++)
    {
        if(p.getChave() == postos[i])
            return i;
    }
    
    return -1;
}

int MatrizDist::novoPosto(const Posto& p)
{
    if(actual >= tamanho)
        aumentarTamanho();

    postos[actual] = p.getChave();

    distancias[actual][actual] = &distanciaZero;

    actual++;
    return actual-1;
}

//cria a matriz com todas posições a apontar para a distancia nula
Distancia*** MatrizDist::criarMatriz(int tamanho)
{
    Distancia*** matriz = new Distancia**[tamanho];
    for(int i=0; i<tamanho; i++)
        matriz[i] = new Distancia*[tamanho];

    for(int i=0; i<tamanho; i++)
    {
        for(int j=0; j<tamanho; j++)
            matriz[i][j]=&distanciaNula;
    }
    return matriz;
}

const MatrizDist& MatrizDist::operator=(const MatrizDist& m)
{
    for(int i = 0; i < actual; i++)
    {
        if(simetrica)
        {
            for(int j=0; j<actual;j++)
            {
                if((distancias[i][j] != &distanciaNula) && (distancias[i][j] != &distanciaZero))
                    delete distancias[i][j];
            }
        }else
        {
            for(int j=0; j<actual;j++)
            {
                if((distancias[i][j] != &distanciaNula) && (distancias[i][j] != &distanciaZero))
                    delete distancias[i][j];
            }
        }
    }

    tamanho = m.tamanho;
    actual = m.actual;
    simetrica = m.simetrica;

    distancias = criarMatriz(tamanho);
    postos = new int[tamanho];

    for(int i = 0; i < actual; i++)
    {
        for(int j = 0;j < actual; j++)
        {
            if((distancias[i][j] != &distanciaNula) && (distancias[i][j] != &distanciaZero))
                distancias[i][j] = m.distancias[i][j]->clone();
            else
                distancias[i][j] = m.distancias[i][j];
        }
    }

    for(int i = 0; i < actual; i++)
        postos[i] = m.postos[i];

    return *this;
}

ostream& MatrizDist::escreve(ostream& out) const
{
    out << "Chave\t";
    for(int i=0; i<actual; i++)
        out << postos[i] << "\t";
    out << endl;
    for(int i=0; i<=actual; i++)
        out <<"---\t";
    out << endl;
    for(int i=0; i<actual; i++)
    {
        out << postos[i] <<"\t";
        for(int j=0; j<actual; j++)
        {
            if(*distancias[i][j] == distanciaNula)
                out << "-\t";
            else
                out << *distancias[i][j] << "\t";
        }
        out << endl;
    }
    return out;
}

bool MatrizDist::vazia() const
{
    return actual == 0;
}

ostream& operator << (ostream& out, const MatrizDist& m)
{
    return m.escreve(out);
}
