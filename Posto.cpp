#include "Posto.h"
#include "PostoArmazem.h"
#include "PostoAutomatico.h"
#include "Pedido.h"


Posto::Posto():Componente(){
    stockActual=0;
}

Posto::Posto(int chave,int stockActual) : Componente(chave)
{
    this->stockActual = stockActual;
}

Posto::Posto(const Posto& p) : Componente(p)
{
    this->stockActual = p.stockActual;
}

Posto::~Posto(){}

void Posto::setStockActual(int stock)
{
    this->stockActual = stock;
}

int Posto::getStockActual() const
{
    return stockActual;
}

void Posto::listar(ostream& out) const
{
    out << "[" << getChave() << "]: ";
    for(int i = 0; (i < stockActual && i < 95) ; i++)
    {
        out << "|";
        if(i==94)
            out << ">";
    }
    out << "(" << stockActual << ")";
}

bool Posto::isOcupado() const
{
    return (stockActual > 0);
}

bool Posto::operator==(const Posto& p) const
{
    return this->stockActual == p.stockActual;
}

bool Posto::operator>(const Posto& p) const
{
    return this->stockActual > p.stockActual;
}

const Posto& Posto::operator=(const Posto& p)
{
    Componente::operator =(p);
    this->stockActual = p.stockActual;
    return *this;
}

ostream& operator << (ostream& out, const Posto& p)
{
    p.listar(out);
    return out;
}
