#ifndef PriorityQueue_
#define PriorityQueue_

#include "Queue.h"

template<class T>
class PriorityQueue : public Queue <T>
{
public:
		PriorityQueue();
		PriorityQueue(const PriorityQueue<T>& q);
		virtual ~PriorityQueue();
        PriorityQueue<T>& operator = (const PriorityQueue<T>& q);
		virtual void insere(const T& elem);
};

template<class T>
PriorityQueue<T>::PriorityQueue() : Queue<T>::Queue()
{
}

template<class T>
PriorityQueue<T>::PriorityQueue(const PriorityQueue<T>& q) : Queue<T>::Queue(q)
{
}

template<class T>
PriorityQueue<T>::~PriorityQueue()
{
}

template<class T>
PriorityQueue<T>& PriorityQueue<T>::operator = (const PriorityQueue<T>& q){
    T elem ;
	if (this == &q)
		return *this ;
	else
	{
		PriorityQueue<T> aux(q) ;
        
        Queue<T>::destroiQueue();
		
		while (!aux.vazia())
		{
			aux.retira(elem);
			insere (elem) ;
		}
		return *this;
	}
}


template<class T>
void PriorityQueue<T>::insere(const T& elem)
{
	Queue <T> qtemp = *this;
	T el;

	while (!Queue<T>::vazia()) Queue<T>::retira(el);

	while ((!qtemp.vazia()) && (qtemp.frente() >= elem)) {
		qtemp.retira(el);
		Queue<T>::insere(el);
	}

	Queue<T>::insere(elem);

	while (!qtemp.vazia()) {
		qtemp.retira(el);
		Queue<T>::insere(el);
	}
}


#endif
