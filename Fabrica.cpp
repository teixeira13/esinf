#include "Fabrica.h"
#include "PostoArmazem.h"
#include "IOFabrica.h"
#include "Lista_Iterador.h"
#include "Pedido.h"
#include "Distancia.h"
#include "PostoAutomatico.h"
#include <string>
#include <sstream>
#include <typeinfo>

Fabrica::Fabrica(){}

Fabrica::Fabrica(const Fabrica& f)
{
    postos = f.postos;
    robots = f.robots;
    distancias = f.distancias;
    emTrabalho = f.emTrabalho;
    minComprimento = f.minComprimento;
    minTempo = f.minTempo;
    criarGrafo();
}

Fabrica::~Fabrica(){}

string Fabrica::setupFabrica(const string& file1,const string& file2,const string& file3,const string& file4)
{
    stringstream str;
    str << IOFabrica::lerPostosArmazem(postos,robots,file1);
    str << IOFabrica::lerPostosAutomaticos(postos,file2);
    str << IOFabrica::lerRobots(robots,postos,file3);
    str << IOFabrica::lerDistancias(distancias,postos,file4);
    criarGrafo();
    criarMatrizesMinimas();
    iniciaTrabalho();
    return str.str();
}

void Fabrica::escreve(ostream& out) const
{
    out << "##################################################PEDIDOS##################################################" << endl;
    if(!pedidos.vazia())
    {
        PriorityQueue<Pedido> temp = pedidos;
        int x= pedidos.comprimento();
        for(int i=0; i<x;i++){
            Pedido tmp;
            temp.retira(tmp);
            out << i+1 << ": " << tmp << endl;
        }

    }else
        out << "Não há pedidos!" << endl;
    Lista_Iterador<Posto*> itp(postos);
    out << "##################################################POSTOS##################################################" << endl;
    while(!itp.fim())
    {
        out << (*(*itp)) << endl;
        itp++;
    }
    out << endl;
    Lista_Iterador<Robot*> itr(robots);
    out << "##################################################ROBOTS##################################################" << endl;
    while(!itr.fim())
    {
        out << (*(*itr)) << endl;
        itr++;
    }
}

const MatrizDist Fabrica::getMatrizMinComprimento() const
{
    return minComprimento;
}

const MatrizDist Fabrica::getMatrizMinTempo() const
{
    return minTempo;
}

const MatrizDist Fabrica::getMatriz() const
{
    return distancias;
}

const Lista<Posto*> Fabrica::getPostos() const
{
    return postos;
}

const Lista<Robot*> Fabrica::getRobots() const
{
    return robots;
}

const Grafo& Fabrica::getGrafo() const
{
    return grafo;
}

void Fabrica::step(int time)
{
    //Envia robots parados e sem stock carregar ao posto armazem mais proximo
    carregarRobots(time);
    //Atende pedidos da fila
    atendePedidos(time);
    //Chama o step dos seus componentes
    Lista_Iterador<Componente*> it(emTrabalho);
    for(int i = 1; i <= emTrabalho.comprimento(); i++)
    {
        (*it)->step(*this,time);
        if(typeid(*(*it)) == typeid(Robot) && !((*it)->isOcupado()))
        {
            Componente* c;
            emTrabalho.remove(i,c);
            i--;
        }
        it++;
    }
}

void Fabrica::carregarRobots(int time)
{
    //Envia robots parados e sem stock carregar ao posto armazem mais proximo
    Lista_Iterador<Robot*> itr(robots);
    while(!itr.fim())
    {
        if(!(*itr)->isOcupado() && (*itr)->getCapacidadeActual() == 0)
        {
            Distancia temp(-1,-1);
            bool valida = false;
            Lista_Iterador<Posto*> itp(postos);
            Posto * p = NULL;
            while(!itp.fim())
            {
                if(typeid(*(*itp)) == typeid(PostoArmazem))
                {
                    PostoArmazem* pa = dynamic_cast<PostoArmazem*>(*itp);
                    if((*itp)->getChave() != (*itr)->getPosicao().getChave() && 
                       (valida == false || temp > minComprimento.getDistancia((*itr)->getPosicao(),*(*itp))) &&
                       !pa->isEmEspera())
                    {
                        temp = minTempo.getDistancia((*itr)->getPosicao(),*(*itp));
                        valida = true;
                        p = pa;
                    }
                }
                itp++;
            }
            if(p != NULL)
            {
                (*itr)->arranca(*this,p,time);
                emTrabalho.insere(1,*itr);
            }
        }
        itr++;
    }
}

void Fabrica::atendePedidos(int time)
{
    Lista<Robot*> naoOcupados;
    Lista_Iterador<Robot*> it1(robots);
    while(!it1.fim())
    {
        if(!(*(*it1)).isOcupado())
        {
            Lista_Iterador<Robot*> it3(naoOcupados);
            int i = 1;
            bool flag = false;
            while(!it3.fim())
            {
                if((*it3)->getCapacidadeActual() < (*it1)->getCapacidadeActual())
                {
                    naoOcupados.insere(i,*it1);
                    flag = true;
                }
                i++;
                it3++;
            }
            if(!flag)
                naoOcupados.insere(i,*it1);
        }
            
        it1++;
    }
    
    while(naoOcupados.comprimento()>0 && !pedidos.vazia())
    {
        Pedido temp;
        pedidos.retira(temp);
        temp.setLock(true);

        while(temp.getQuantidadeRequisicao() > 0 && naoOcupados.comprimento()>0)
        {
            Distancia d(-1,-1);
            Lista_Iterador<Robot*> it2(naoOcupados);
            int pr;
            bool valida = false;
            for(int c = 1; c <= naoOcupados.comprimento();c++)
            {
                if(typeid(temp.getPosto()) == typeid(PostoArmazem))
                {
                    Distancia::setComparacao(Distancia::Comprimento);
                    if(valida == false || d > minComprimento.getDistancia((*it2)->getPosicao(),*temp.getPosto()))
                    {
                        d = minComprimento.getDistancia((*it2)->getPosicao(),*temp.getPosto());
                        valida = true;
                        pr = c;
                    }
                }
                else
                {
                    Distancia::setComparacao(Distancia::Tempo);
                    if(valida == false || d > minTempo.getDistancia((*it2)->getPosicao(),*temp.getPosto()))
                    {
                        d = minTempo.getDistancia((*it2)->getPosicao(),*temp.getPosto());
                        valida = true;
                        pr = c;
                    }    
                }
                it2 ++;
            }
            Robot* r;
            naoOcupados.remove(pr,r);
            r->arranca(*this,temp.getPosto(),time);
            temp.insereRobot(*r);
            emTrabalho.insere(1,r);
            int x = temp.getQuantidadeRequisicao();
            x = (x-r->getCapacidadeActual()<0)? 0 : x-r->getCapacidadeActual();
            temp.setQuantidadeRequisicao(x);
        }

        if(temp.getQuantidadeRequisicao() != 0)
            pedidos.insere(temp);
        else
        {
            temp.setTimeDespachado(time);
            historico.insere(temp);
        }
    }
}

void Fabrica::inserePedido(const Pedido& p)
{
    PriorityQueue<Pedido> temp = pedidos;
    bool flag = false;
    while (!temp.vazia()) {
        Pedido tmp;
        temp.retira(tmp);
        if(tmp.getPosto()->getChave() == p.getPosto()->getChave())
            flag = true;
    }
    if(!flag)
        pedidos.insere(p);
}

void Fabrica::arquivaPedido(const Pedido& p)
{
    historico.insere(p);
}

bool Fabrica::operator >(const Fabrica& f) const
{
    return postos.comprimento() > f.postos.comprimento();
}

const Fabrica& Fabrica::operator =(const Fabrica& f)
{
    postos = f.postos;
    robots = f.robots;
    distancias = f.distancias;
    return *this;
}

bool Fabrica::vazia() const
{
    return postos.vazia() && robots.vazia() && distancias.vazia();
}

void Fabrica::criarGrafo()
{
    grafo.juntarVertices(postos);
    grafo.juntarRamos(postos,distancias);
}

void Fabrica::criarMatrizesMinimas()
{
    Lista_Iterador<Posto*> it1(postos);
    while(!it1.fim())
    {
        Posto* p1 = *it1;
        Lista_Iterador<Posto*> it2(postos);
        while(!it2.fim())
        {
            Posto* p2 = *it2;
            Stack<Posto*> temp;
            Distancia d;
            d = grafo.Dijkstra(p1->getChave(),p2->getChave(),temp,Distancia::Comprimento);
            minComprimento.insere(d,*p1,*p2);
            d = grafo.Dijkstra(p1->getChave(),p2->getChave(),temp,Distancia::Tempo);
            minTempo.insere(d,*p1,*p2);
            it2++;
        }
        it1++;
    }
}


void Fabrica::iniciaTrabalho()
{
    Lista_Iterador<Posto*> it(postos);
    while(!it.fim())
    {
        emTrabalho.insere(1,*it);
        it++;
    }
}

bool Fabrica::validaGrafo() const{
    
    Lista_Iterador<Posto*> it(postos);
    bool validado=true;
    while(!it.fim()){
        int chave=(*it)->getChave();
        if (typeid(*(*it))!=typeid(PostoArmazem)){
            validado=grafo.visita_largura(chave);
            if (!validado) return false;
        }
        it++;
    }
    return validado;
}

const PriorityQueue<Pedido>& Fabrica:: getPedidos() const{
    return pedidos;
}

const Queue<Pedido>& Fabrica::getHistoricoPedidos() const
{
    return historico;
}

ostream& operator << (ostream& out,const  Fabrica& f)
{
    f.escreve(out);
    return out;
}
