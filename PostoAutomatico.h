#ifndef POSTOAUTOMATICO_H
#define POSTOAUTOMATICO_H

#include "Posto.h"

class Fabrica;


class PostoAutomatico : public Posto
{
    public:
        PostoAutomatico();
        PostoAutomatico(int chave,int stockActual,int stockRequisitar, float velOperacao);
        PostoAutomatico(const PostoAutomatico& p);
        ~PostoAutomatico();

        int getStockRequesitar() const;
        float getvelOperacao() const;
        void setStockRequesitar(int stockRequesitar);
        void setVelOperacao(float velOperacao);
        bool isOcupado() const;
        void step(Fabrica& f,int time);
        int action(int stockMaxRobot,int stockActualRobot);
        PostoAutomatico* clone() const;

        const PostoAutomatico& operator=(const PostoAutomatico& p);
        void listar(ostream& out) const;
    private:
        int stockRequisitar;
        float velOperacao;
};

ostream& operator << (ostream& out, const PostoAutomatico& p);



#endif // POSTOAUTOMATICO_H
