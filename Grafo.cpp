#include "Grafo.h"
#include "Fabrica.h"
#include "vector"
#include "PostoArmazem.h"
#include <typeinfo>
using namespace std;


Grafo::Grafo(){}

Grafo::Grafo(const Grafo& g):ListAdjGrafo<Posto*,Distancia>(g){}

Grafo::~Grafo(){}

void Grafo::juntarVertices(const Lista<Posto*> &temp)
{
  //Lista<Posto*> temp = f.getPostos();
  Lista_Iterador<Posto*> it(temp);
  while(!it.fim())
  {
    juntar_vertice(*it);
    it++;
  }
}

void Grafo::juntarRamos(const Lista<Posto*> &temp, const MatrizDist &matriz)
{
  //Lista<Posto*> temp = f.getPostos();
  Distancia nula(-1,-1);
  Distancia zero(0,0);
  Distancia d;
  Lista_Iterador<Posto*> it1(temp);

  while(!it1.fim())
  {
    Lista_Iterador<Posto*> it2(temp);
    while(!it2.fim())
    {
      d = matriz.getDistancia(**it1,**it2);
      if(d != nula && d != zero)
      {
          juntar_ramo(d,*it1,*it2);
      }
      it2 ++;
    }
    it1++;
  }
}
 

  // Método de pesquisa do distancia mínima pesado (Dijkstra).
const Distancia& Grafo::Dijkstra(const int &chaveInicio, const int &chaveFim,Stack<Posto*>& sCaminho,Distancia::Comparacao comp) const
{
    Vertice<Posto*,Distancia>* pInicio;
    Vertice<Posto*,Distancia>* pFim;
    pInicio = procuraPosto(chaveInicio);
    pFim = procuraPosto(chaveFim);
    Distancia zero(0,0);
    if(comp == Distancia::Comprimento)
        Distancia::setComparacao(Distancia::Comprimento);
    else
        Distancia::setComparacao(Distancia::Tempo);
        
    if (pInicio == NULL || pFim == NULL)
    {
        //cout << "[ERRO] DIJKSTRA: Pelo menos um dos vértices não existe!!\n\n";
//       return NULL;
    }
    
    vector<Distancia> distancia(nvertices+1);
    vector<bool> distanciaValida(nvertices+1, false);
    vector<Posto*> antecessor(nvertices+1, pInicio->GetConteudo());
    vector<bool> processado(nvertices+1, false);
    
    distancia[pInicio->GetKey()] = zero;  
    distanciaValida[pInicio->GetKey()] = true;
    
    Vertice<Posto*,Distancia>* pActual = pInicio;
    
    // Enquanto houver vértices alcançáveis...
    while(pActual != NULL)
    {
        processado[pActual->GetKey()] = true;  // Vértice processado.
        
        Ramo<Posto*,Distancia>* pRamo = pActual->GetRamo();
        
        // Enquanto houver vértices adjacentes...
        while(pRamo != NULL)
        {
            // Se o vértice ainda não tiver sido considerado processado...
            if (processado[pRamo->GetVertice()->GetKey()] == false)
            {
                Distancia x = distancia[pActual->GetKey()];
                if (distanciaValida[pRamo->GetVertice()->GetKey()] == false ||
                    distancia[pRamo->GetVertice()->GetKey()] > x + pRamo->GetConteudo())
                {
                    Distancia y = distancia[pActual->GetKey()];
                    distancia[pRamo->GetVertice()->GetKey()] = y + pRamo->GetConteudo();
                    distanciaValida[pRamo->GetVertice()->GetKey()] = true;
                    antecessor[pRamo->GetVertice()->GetKey()] = pActual->GetConteudo();
                }
            }
            // Para o próximo ramo adjacente.
            pRamo = pRamo->GetRamo();
        }
        
        // Qual o nó por processar com menor distância à origem?
        int imin = 0;
        for(int i = 1; i <= nvertices; i++)
        {
            if (processado[i] == false && distanciaValida[i] == true)
                if (imin == 0 || distancia[i] < distancia[imin])
                {
                    imin = i;
                }
        }
        
        // Segue a partir do vértice mais próximo da origem, por processar.
        pActual = encvert_key(imin);
        
    }
    
    // Já se visitaram todos os vértices alcançáveis...
    
    // Estabelecer o caminho mínimo entre os dois vértices, a partir do destino.
    // Verifica-se primeiro se o vértice final é alcançavel a partir do vertice inicial.
    if (processado[pFim->GetKey()] == true)
    {
        int keyActual = pFim->GetKey();
        
        sCaminho.push(pFim->GetConteudo());
        
        do {
            Vertice<Posto*,Distancia>* pAnterior = encvert_conteudo(antecessor[keyActual]);
            sCaminho.push(pAnterior->GetConteudo());
            keyActual = encvert_conteudo(antecessor[keyActual])->GetKey();
        } while (keyActual != pInicio->GetKey());
    }
    return distancia[pFim->GetKey()];
}
    
    

//adaptacao do enc_vert
Vertice<Posto* ,Distancia>* Grafo::procuraPosto(const int& chave) const
{
    Vertice<Posto*,Distancia>* ap=graf;

    while (ap != NULL)
    {
            if (ap->GetConteudo()->getChave() == chave)//chave do pp posto, e nao do vertice
                    return ap;
            else
                    ap = ap->GetVertice();
    }
    return ap;
}


bool Grafo::visita_largura(const int &chaveInicio) const
{
    
    Vertice<Posto*,Distancia>* pInicio;
    pInicio = procuraPosto(chaveInicio);
    
    if (pInicio == NULL) {
        return false;
    }
    vector<bool> vVisitados(nvertices+1, false);
    queue<Vertice<Posto*, Distancia>*> qProcessar;
    
    qProcessar.push(pInicio);
    vVisitados[pInicio->GetKey()] = true;
    
    while (!qProcessar.empty()) {
        Vertice<Posto*, Distancia> *pVertice;
        
        pVertice= qProcessar.front();
        qProcessar.pop();
        if (typeid(*(pVertice->GetConteudo()))==typeid(PostoArmazem)){
            return true;
        }
        Ramo<Posto*, Distancia>*pRamo = pVertice->GetRamo();
        
        while (pRamo != NULL) {
            if (vVisitados[pRamo->GetVertice()->GetKey()] == false) {
                qProcessar.push(pRamo->GetVertice());
                vVisitados[pRamo->GetVertice()->GetKey()] = true;
            }
            pRamo = pRamo->GetRamo();
        }
    }
    return false;
}



void Grafo::escreve() const{
    Vertice<Posto*, Distancia>* v = graf;
    Ramo<Posto*, Distancia>* r;

    if (v == NULL)
        cout << "Grafo nao definido !" << endl;
    else {
        cout << "Num vertices " << nvertices << endl;
        cout << "Num ramos " << nramos << endl;

        while (v != NULL) {
            cout << "O vertice " << *(v->GetConteudo()) << " liga-se a: " << endl;
            r = v->GetRamo();
            while (r) {
                cout << *(r->GetVertice()->GetConteudo()) << "  ";
                cout << " Conteudo -> " << r->GetConteudo() << endl;
                r = r->GetRamo();
            }
            cout << endl;
            v = v->GetVertice();
        }
    }
} 
