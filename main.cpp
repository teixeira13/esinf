#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include "unistd.h"
#include "IOFabrica.h"
#include "Distancia.h"
#include "MatrizDist.h"
#include "Posto.h"
#include "Queue.h"
#include "Lista_Iterador.h"
#include "Fabrica.h"
#include "Pedido.h"

using namespace std;

void menu1(Fabrica &f)
{
    int i = 1;
    string files[4], x;
    stringstream in;
    cout << "Indique a pasta onde se encontram os ficheiros" << endl;
    cout << "Caminho: ";
    cin >> x;
    in << x << "/";
    while(i < 5)
    {
        stringstream temp;
        temp << in.str();
        temp << "FX" << i << ".csv";
        ifstream file(temp.str().c_str());
        if(file.is_open())
        {
            files[i-1] = temp.str();
            i++;
            file.close();
        }else
        {
            cout << "CAMINHO INVÁLIDO" << endl;
            break;
        }
    }
    if(i > 4)
        cout << endl << endl << f.setupFabrica(files[0].c_str(),files[1].c_str(),files[2].c_str(),files[3].c_str()) << endl;
}

int main()
{
    Fabrica* f = new Fabrica();
    int i;
    int time;
    int automatico;
    int t, c, x;
    
    PriorityQueue<Pedido> temp;
    Queue<Pedido> tmp;
    
    do{
        cout << "\nEscolha uma das seguintes opções:" << endl;
        if(!f->vazia())
        {
            cout << "1 - Refazer fabrica" << endl;
            cout << "2 - Consultar Estado da Fabrica" << endl
                 << "3 - Validar se todos os postos automáticos são alcançáveis a partir de pelo menos um posto armazém" << endl
                 << "4 - Simular Funcinamento da Fábrica" << endl
                 << "5 - Mostrar todos os pedidos da fila de pedidos" << endl
                 << "6 - Mostar histórico de pedidos atendidos" << endl
                 << "0 - Sair" << endl;
        }
        else
        {
            cout << "1 - Setup Fabrica" << endl
            << "0 - Sair" << endl;
        }
        cin >> i;
        
        if(i < 0 || i > 6)
        {
            cout << "Introduza uma opção válida" << endl;
            continue;
        }
        
        switch(i)
        {
            case 1:
                if(!f->vazia())
                {
                    delete f;
                    f = new Fabrica();
                }
                menu1(*f);
                time = 0;
                break;
            case 2:
                cout << *f;
                break;
            case 3:
                if (f->validaGrafo())
                {
                    cout<<"Todos os postos automáticos são alcançáveis a partir de pelo menos um posto armazém."<<endl;
                }
                else
                {
                    cout<<"Existe pelo menos um posto automático que não é alcançável a partir de qualquer posto armazém."<<endl;
                }
                break;
            case 4:
                c=0;
                cout << "Quantos minutos deseja simular? ";
                cin >> t;
                cout << "1 - Simular Manualmente" << endl;
                cout << "2 - Avançar directamente " << t << " minutos" << endl;
                cin >> automatico;
                while(c <= t)
                {
                    cout << "\033[2J\033[;H";
                    cout << "Minuto: " << time << endl;
                    cout << *f;
                    time++;
                    f->step(time);
                    c++;
                    if (automatico == 1)
                        cin.get();
                    else
                        sleep(1);
                }
                cout << "\033[2J\033[;H";
                break;
            case 5:
                temp = f->getPedidos();
                x = temp.comprimento();
                if(x == 0)
                    cout << "Não existem pedidos em espera" << endl;
                else
                    for(int i=0; i<x;i++){
                        Pedido tmp;
                        temp.retira(tmp);
                        cout << i+1 << ": " << tmp << endl;
                    }
                break;
            case 6:
                tmp = f->getHistoricoPedidos();
                x = tmp.comprimento();
                for(int i = 0; i<x;i++)
                {
                    Pedido p;
                    Queue<Robot> tmpr;
                    tmp.retira(p);
                    tmpr = p.getHistorico();
                    cout << "Pedido realizado pelo posto[" << p.getPosto()->getChave() << "] no minuto " << p.getTimePedido() << ", dado como despachado no minuto " << p.getTimeDespachado() << endl;
                    cout << "Robots Utilizados:" << endl;
                    while(!tmpr.vazia())
                    {
                        Robot r;
                        tmpr.retira(r);
                        cout << "[" << r.getChave() << "]";
                    }
                    cout << endl;
                }
        }
    }while(i!=0);

    
    
    
    return 0;
}
