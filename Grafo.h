//
//  Grafo.h
//  esinf
//
//  Created by Joana Verde on 11/3/12.
//  Copyright (c) 2012 Joana Verde. All rights reserved.
//

#ifndef esinf_Grafo_h
#define esinf_Grafo_h
#include <iostream>
#include "ListAdjGrafo.h"
#include "Queue.h"
#include "Lista_Iterador.h"
#include "Posto.h"
#include "Distancia.h"
#include "MatrizDist.h"
#include "Grafo.h"
#include "Lista.h"
#include "Stack.h"
#include "Queue.h"
using namespace std;


class Fabrica;

using namespace std;

class Grafo: public ListAdjGrafo<Posto*, Distancia>
{
public:
    Grafo();
    Grafo(const Grafo& g);
    ~Grafo();
    const Grafo& operator=(const Grafo& g);
    bool operator == (const Grafo& g);
    
    void juntarVertices(const Lista<Posto*> &temp);
    void juntarRamos(const Lista<Posto*> &temp, const MatrizDist &matriz);
    const Distancia& Dijkstra(const int &chaveInicio, const int &chaveFim,Stack<Posto*>& sCaminho,Distancia::Comparacao comp) const;
    Vertice<Posto* ,Distancia>* procuraPosto(const int& chave) const;
    void escreve() const;
    bool visita_largura(const int &chaveInicio) const;

};
#endif
    
    
   

    
    
